#derive HES diagnoses of diabetes
library(tidyverse)

#load icd data
df_icd <- read.csv("../../raw_data/biobank_dload/ukb671072_icddx.csv", stringsAsFactors = FALSE)
df_icd$eid<-as.character(df_icd$eid)
#load id list. use those with structural idps
load("../../derivatives/df_analysis_structural_ids.rda")
df_analysis_structural_ids$eid<-as.character(df_analysis_structural_ids$eid)

#lets subset the icd df, only work with the imaging sample
#much smaller, faster
df_analysis_icd <- list(df_analysis_structural_ids, df_icd) %>% reduce(left_join, by="eid")
rm(df_icd)

sapply(df_analysis_icd, class)
#some diagnosis fileds are integers/not characters. convert to chars
for (x in seq(2,291)){ # 2-244 are the icd10 code fields; #245 - 291 are icd9
  df_analysis_icd[,x] <- as.character(df_analysis_icd[,x])
}

#date fields are mostly characters. convert to date
for (x in seq(292,581)){
  df_analysis_icd[,x] <- as.Date(df_analysis_icd[,x], format="%Y-%m-%d")
}
sapply(df_analysis_icd, class)

#### DERIVE DIAGNOSIS ####

extract_icd_diagnosis<- function(df_icd, codes_to_extract){
  #function to search through icd code columns for a given list of codes
  #matches based on containing the string given, not exact match
  #eg E11 will match to E11, E112, E119
  #returns a n x 2 vec
  #row 1 = 0/1 for no/yes to code found
  #row 2 = date (if 1)
  
  ltc_diagnosis_date<-apply(df_icd[,-1], 1, function(x){
    #use grepl to search for the codes, then use which to find in which columns they exist
    #this is integer(0) if no code in a row, else it is the diagnosis column number
    codes_position <- which(grepl(paste(codes_to_extract, collapse="|"),x)) 
    if(length(codes_position)>0){ #are any of the codes in this row
      codes_position <- codes_position + 290 #offset, dates start at 291 of df_icd[,-1]
      ltc_dates <- x[codes_position] #get list of dates corresponding to the ltc codes
      return(c(1,min(ltc_dates))) #1 for diagnosis column, minimum date for date column
      #return(1)
    } else {
      return(c(0,0))
    }})
  return(ltc_diagnosis_date)
}
  
#### T2 Diabetes ####
t2_data<-extract_icd_diagnosis(df_analysis_icd, c("E11"))
df_analysis_icd$HES_diagnosis_of_T2diabetes<-as.integer(t2_data[1,])
df_analysis_icd$HES_diagnosis_date_of_T2diabetes<-as.Date(t2_data[2,], format="%Y-%m-%d")
#check
View(df_analysis_icd[,c("eid","HES_diagnosis_of_T2diabetes","HES_diagnosis_date_of_T2diabetes",
                        "X41270.0.0","X41270.0.1","X41270.0.2","X41270.0.3","X41270.0.4",
                        "X41280.0.0","X41280.0.1","X41280.0.2","X41280.0.3","X41280.0.4")])
#there are some who have a diagnosis but not a date
summary(as.factor(df_analysis_icd$HES_diagnosis_of_T2diabetes))
#0     1 
#40621  1680 
summary(df_analysis_icd$HES_diagnosis_date_of_T2diabetes)
#Min.      1st Qu.       Median         Mean      3rd Qu.         Max.         NA's 
#"1997-05-04" "2011-04-26" "2015-06-10" "2014-07-22" "2018-10-23" "2021-09-21"      "40622" 
#ok its actually only 1 (40622 - 40621) but dont take this dx anyway
df_analysis_icd$HES_diagnosis_of_T2diabetes[is.na(df_analysis_icd$HES_diagnosis_date_of_T2diabetes)] <- 0
summary(as.factor(df_analysis_icd$HES_diagnosis_of_T2diabetes))
#0     1 
#40622  1679
rm(t2_data)

#### T1 Diabetes ####
t1_data<-extract_icd_diagnosis(df_analysis_icd, c("E10"))
df_analysis_icd$HES_diagnosis_of_T1diabetes<-as.integer(t1_data[1,])
df_analysis_icd$HES_diagnosis_date_of_T1diabetes<-as.Date(t1_data[2,], format="%Y-%m-%d")
df_analysis_icd$HES_diagnosis_of_T1diabetes[is.na(df_analysis_icd$HES_diagnosis_date_of_T1diabetes)] <- 0
summary(as.factor(df_analysis_icd$HES_diagnosis_of_T1diabetes))
#0     1 
#42133   168 
rm(t1_data)

#### other Diabetes ####
other_data<-extract_icd_diagnosis(df_analysis_icd, c("E13","E14"))
df_analysis_icd$HES_diagnosis_of_diabetes_other<-as.integer(other_data[1,])
df_analysis_icd$HES_diagnosis_date_of_diabetes_other<-as.Date(other_data[2,], format="%Y-%m-%d")
df_analysis_icd$HES_diagnosis_of_diabetes_other[is.na(df_analysis_icd$HES_diagnosis_date_of_diabetes_other)] <- 0
summary(as.factor(df_analysis_icd$HES_diagnosis_of_diabetes_other))
# 0     1 
# 42095   206 
rm(other_data)

#### pre diabetic ####
prediab_data<-extract_icd_diagnosis(df_analysis_icd, c("R73"))
df_analysis_icd$HES_diagnosis_of_prediabetes<-as.integer(prediab_data[1,])
df_analysis_icd$HES_diagnosis_date_of_prediabetes<-as.Date(prediab_data[2,], format="%Y-%m-%d")
df_analysis_icd$HES_diagnosis_of_prediabetes[is.na(df_analysis_icd$HES_diagnosis_date_of_prediabetes)] <- 0
summary(as.factor(df_analysis_icd$HES_diagnosis_of_prediabetes))
# 0     1 
# 42153   148 
rm(prediab_data)


#### any diabetes ####
#lets make a var that is 1 if any of t1,t2 etc diabetes =1 
any_data<-extract_icd_diagnosis(df_analysis_icd, c("E10","E11","E12","E13","E14","O24"))
df_analysis_icd$HES_diagnosis_of_diabetes<-as.integer(any_data[1,])
df_analysis_icd$HES_diagnosis_date_of_diabetes<-as.Date(any_data[2,], format="%Y-%m-%d")
df_analysis_icd$HES_diagnosis_of_diabetes[is.na(df_analysis_icd$HES_diagnosis_date_of_diabetes)] <- 0
summary(as.factor(df_analysis_icd$HES_diagnosis_of_diabetes))
# 0     1 
# 40469  1832 
rm(any_data)
                                                      


#now which are at baseline?
load("../../derivatives/df_analysis_structural.rda")
names(df_analysis_structural)
sapply(df_analysis_structural, class)

#make date vars dates
for (x in seq(20,23)){
  df_analysis_structural[,x] <- as.Date(df_analysis_structural[,x], format="%Y-%m-%d")
}
sapply(df_analysis_structural, class)
  
#compare diagnosis date to assessment date
df_analysis_icd<-list(df_analysis_icd, df_analysis_structural) %>% reduce(left_join, by="eid")

df_analysis_icd$HES_diagnosis_of_T2diabetes_0_0 <- as.factor(
  ifelse(is.na(df_analysis_icd$HES_diagnosis_date_of_T2diabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0),0,
         ifelse((df_analysis_icd$HES_diagnosis_date_of_T2diabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0) <=0,1,0)))
#check
View(df_analysis_icd[,c("eid","HES_diagnosis_of_T2diabetes","HES_diagnosis_date_of_T2diabetes",
                        "Date_of_attending_assessment_centre_2_0","HES_diagnosis_of_T2diabetes_0_0")])
#good
#n?
summary(df_analysis_icd$HES_diagnosis_of_T2diabetes_0_0)
# 0     1 
# 41179  1122 

df_analysis_icd$HES_diagnosis_of_T1diabetes_0_0 <- as.factor(
  ifelse(is.na(df_analysis_icd$HES_diagnosis_date_of_T1diabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0),0,
         ifelse((df_analysis_icd$HES_diagnosis_date_of_T1diabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0) <=0,1,0)))
summary(df_analysis_icd$HES_diagnosis_of_T1diabetes_0_0)
#0     1 
#42181   120 

df_analysis_icd$HES_diagnosis_of_diabetes_other_0_0 <- as.factor(
  ifelse(is.na(df_analysis_icd$HES_diagnosis_date_of_diabetes_other - df_analysis_icd$Date_of_attending_assessment_centre_2_0),0,
         ifelse((df_analysis_icd$HES_diagnosis_date_of_diabetes_other - df_analysis_icd$Date_of_attending_assessment_centre_2_0) <=0,1,0)))
summary(df_analysis_icd$HES_diagnosis_of_diabetes_other_0_0)
#0     1 
#42158   143 


df_analysis_icd$HES_diagnosis_of_diabetes_0_0 <- as.factor(
  ifelse(is.na(df_analysis_icd$HES_diagnosis_date_of_diabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0),0,
         ifelse((df_analysis_icd$HES_diagnosis_date_of_diabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0) <=0,1,0)))
summary(df_analysis_icd$HES_diagnosis_of_diabetes_0_0)
#0     1 
#41038  1263 

df_analysis_icd$HES_diagnosis_of_prediabetes_0_0 <- as.factor(
  ifelse(is.na(df_analysis_icd$HES_diagnosis_date_of_prediabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0),0,
         ifelse((df_analysis_icd$HES_diagnosis_date_of_prediabetes - df_analysis_icd$Date_of_attending_assessment_centre_2_0) <=0,1,0)))
summary(df_analysis_icd$HES_diagnosis_of_prediabetes_0_0)
#0     1 
#42250    51

#for diagnosis of diabetes, compute disease duration
df_analysis_icd$HES_duration_of_diabetes <- ifelse(df_analysis_icd$HES_diagnosis_of_diabetes_0_0==1,
                                                   df_analysis_icd$Date_of_attending_assessment_centre_2_0 - df_analysis_icd$HES_diagnosis_date_of_diabetes,0)
summary(df_analysis_icd$HES_duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 0.00    0.00    0.00   71.74    0.00 8105.00 
hist(df_analysis_icd$HES_duration_of_diabetes)

summary(df_analysis_icd[which(df_analysis_icd$HES_diagnosis_of_diabetes_0_0==1),]$HES_duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 2.0   948.5  2003.0  2402.9  3555.0  8105.0 
hist(df_analysis_icd[which(df_analysis_icd$HES_diagnosis_of_diabetes_0_0==1),]$HES_duration_of_diabetes)

#lets save out the newly created data
df_hes_diagnoses<-df_analysis_icd[,-seq(2,581)] #2 - 581 are icd fields
save(df_hes_diagnoses, file = "../../derivatives/2_df_hes_diagnoses.rda")
rm(df_analysis_icd)


#### SELF REPORT ####
#now lets assess self report diagnosis of diabetes
df_selfreport <- read.csv("../../raw_data/biobank_dload/ukb671072_selfreportdx.csv", stringsAsFactors = FALSE)
names(df_selfreport)
df_selfreport$eid <- as.character(df_selfreport$eid)

#lets take just the imaging sample
df_analysis_selfreport <- list(df_analysis_structural_ids, df_selfreport) %>% reduce(left_join, by="eid")
rm(df_selfreport)

#first few cols are answers to "has doctor ever said you had diabetes". handle these separately
df_analysis_doctor_diabetes <- df_analysis_selfreport[,seq(1,9)]
df_analysis_selfreport<-df_analysis_selfreport[,-seq(2,9)]
sapply(df_analysis_selfreport, class)

#self report fields are either int or logical. convert to characters
for (x in seq(2,137)){ 
  df_analysis_selfreport[,x] <- as.character(df_analysis_selfreport[,x])
}

#self report year fields are either numeric or logical. convert to numeric
for (x in seq(138,273)){ 
  df_analysis_selfreport[,x] <- as.numeric(df_analysis_selfreport[,x])
}

#self report age fields are either integer or logical. convert to integer
for (x in seq(274,409)){ 
  df_analysis_selfreport[,x] <- as.integer(df_analysis_selfreport[,x])
}


#possible sr codes are:
#1220 - diabetes
#1222 - type 1 
#1223 - type 2

#1276 - diabetic eye disease
#1468 - diabetic neuropathy
#1607 - diabetic nephropathy
#we have sr at time 0 (~ 2008) and imaging tpoint (~2016), so get both for each code

#### diabetes 1220 ####
df_analysis_selfreport$selfreport_diabetesgeneral_0_0<-apply(df_analysis_selfreport[,2:35], 1, function(x){
  if(any (x %in% c("1220"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_diabetesgeneral_0_0))
#0     1 
#41421   880 

df_analysis_selfreport$selfreport_diabetesgeneral_2_0<-apply(df_analysis_selfreport[,70:103], 1, function(x){
  if(any (x %in% c("1220"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_diabetesgeneral_2_0))
#0     1 
#41228  1073 



#### t1diabetes 1222 ####
df_analysis_selfreport$selfreport_T1diabetes_0_0<-apply(df_analysis_selfreport[,2:35], 1, function(x){
  if(any (x %in% c("1222"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_T1diabetes_0_0))
#0     1 
#42284    17  

df_analysis_selfreport$selfreport_T1diabetes_2_0<-apply(df_analysis_selfreport[,70:103], 1, function(x){
  if(any (x %in% c("1222"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_T1diabetes_2_0))
#0     1 
#42238    63  


#### t2diabetes 1223 ####
df_analysis_selfreport$selfreport_T2diabetes_0_0<-apply(df_analysis_selfreport[,2:35], 1, function(x){
  if(any (x %in% c("1223"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_T2diabetes_0_0))
#0     1 
#42177   124 

df_analysis_selfreport$selfreport_T2diabetes_2_0<-apply(df_analysis_selfreport[,70:103], 1, function(x){
  if(any (x %in% c("1223"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_T2diabetes_2_0))
#0     1 
#41596   705 


#### diabetes any 1220 1222 1223 1276 1468 1607 ####
df_analysis_selfreport$selfreport_diabetes_0_0<-apply(df_analysis_selfreport[,2:35], 1, function(x){
  if(any (x %in% c("1220","1222","1223","1276","1468","1607"))){ #are any of the codes in this row
    return(1)
  } else {
    return(0)
  }})
summary(as.factor(df_analysis_selfreport$selfreport_diabetes_0_0))
#0     1 
#41282  1019 

#lets get age of diagnosis as well for duration
#df_analysis_selfreport$selfreport_diabetes_2_0<-apply(df_analysis_selfreport[,70:103], 1, function(x){
#  if(any (x %in% c("1220","1222","1223","1276","1468","1607"))){ #are any of the codes in this row
#    return(1)
#  } else {
#    return(0)
#  }})
#summary(as.factor(df_analysis_selfreport$selfreport_diabetes_2_0))
##0     1 
##40476  1825  

sr_diagnosis_date<-apply(df_analysis_selfreport[,-1], 1, function(x){
  codes_to_extract<-c("1220","1222","1223","1276","1468","1607")
  #use grepl to search for the codes, then use which to find in which columns they exist
  #this is integer(0) if no code in a row, else it is the diagnosis column number
  codes_position <- which(grepl(paste(codes_to_extract, collapse="|"),x[69:102])) 
  if(length(codes_position)>0){ #are any of the codes in this row
    codes_position <- codes_position + 340 #offset, ages start at 273 of df_selfreport[,-1]
    dx_age <- x[codes_position] #get list of dates corresponding to the ltc codes
    return(c(1,min(dx_age))) #1 for diagnosis column, minimum date for date column
    #return(1)
  } else {
    return(c(0,0))
  }})
df_analysis_selfreport$selfreport_diabetes_2_0 <- as.integer(sr_diagnosis_date[1,])
df_analysis_selfreport$selfreport_age_at_diabetes_2_0 <-as.numeric(sr_diagnosis_date[2,])
View(df_analysis_selfreport[,c(1,417,418,seq(70,75),seq(341,346))]) #good
summary(as.factor(df_analysis_selfreport$selfreport_diabetes_2_0))

#to get duration we need age at imaging
names(df_analysis_selfreport)
df_analysis_selfreport <- list(df_analysis_selfreport, df_analysis_structural[,c("eid","Age_when_attended_assessment_centre_2_0")]) %>% reduce(left_join, by="eid")
names(df_analysis_selfreport)

df_analysis_selfreport$selfreport_duration_of_diabetes <- 365.25*(ifelse((df_analysis_selfreport$selfreport_diabetes_2_0==1) &(df_analysis_selfreport$selfreport_age_at_diabetes_2_0>=0),
                                                                         df_analysis_selfreport$Age_when_attended_assessment_centre_2_0 -
                                                                           df_analysis_selfreport$selfreport_age_at_diabetes_2_0,0))
summary(df_analysis_selfreport$selfreport_duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 0.0     0.0     0.0   161.5     0.0 24837.0
View(df_analysis_selfreport[,c(1,420,417,418,419)])
View(df_analysis_selfreport[,c(1,420,417,418,seq(70,75),seq(341,346))])
summary(df_analysis_selfreport[which(df_analysis_selfreport$selfreport_diabetes_2_0==1),]$selfreport_duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 0    1096    2922    3742    5114   24837
hist(df_analysis_selfreport[which(df_analysis_selfreport$selfreport_diabetes_2_0==1),]$selfreport_duration_of_diabetes)
View(df_analysis_selfreport[,c(1,420,417,418,419)])
#there are cases where self reported age = age at assessment

summary(df_analysis_selfreport[which(df_analysis_selfreport$selfreport_diabetesgeneral_2_0==1),]$selfreport_age_at_diabetes_2_0)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# -1.00   50.00   57.00   54.31   63.00   79.00 

summary(df_analysis_selfreport[which(df_analysis_selfreport$selfreport_T1diabetes_2_0==1),]$selfreport_age_at_diabetes_2_0)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 6.00   21.00   36.00   35.71   50.00   69.00 
summary(df_analysis_selfreport[which(df_analysis_selfreport$selfreport_T2diabetes_2_0==1),]$selfreport_age_at_diabetes_2_0)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# -1.00   51.00   58.00   56.96   63.00   80.00 

#..can people answer yes to diabetes and t2 diabetes?
nrow(df_analysis_selfreport[which( (df_analysis_selfreport$selfreport_diabetesgeneral_0_0==1) &
                                     (df_analysis_selfreport$selfreport_T2diabetes_0_0==1)),])
#[1] 2

nrow(df_analysis_selfreport[which( (df_analysis_selfreport$selfreport_diabetesgeneral_2_0==1) &
                                     (df_analysis_selfreport$selfreport_T2diabetes_2_0==1)),])
#[1] 17
#so they are mostly exclusive... can we assume 1220 is mostly t2? probably

#how about any who said yes to diabetes at t0 but no at t2
nrow(df_analysis_selfreport[which( (df_analysis_selfreport$selfreport_diabetes_0_0==1) &
                                     (df_analysis_selfreport$selfreport_diabetes_2_0==0)),])
#[1] 168

nrow(df_analysis_selfreport[which( (df_analysis_selfreport$selfreport_T2diabetes_0_0==1) &
                                     (df_analysis_selfreport$selfreport_T2diabetes_2_0==0)),])
#[1] 79



#lets save our selfreport diagnosis columns for now
df_analysis_selfreport<-df_analysis_selfreport[,-c(seq(2,409),419)]
save(df_analysis_selfreport, file="../../derivatives/df_analysis_selfreport.rda")


#### doctor diabetes ####
names(df_analysis_doctor_diabetes)
names(df_analysis_doctor_diabetes)<-c("eid","doctor_diabetes_0_0","doctor_diabetes_1_0",
                                      "doctor_diabetes_2_0","doctor_diabetes_3_0",
                                      "age_doctor_diabetes_0_0","age_doctor_diabetes_1_0",
                                      "age_doctor_diabetes_2_0","age_doctor_diabetes_3_0")

df_analysis_doctor_diabetes$eid <- as.character(df_analysis_doctor_diabetes$eid)

#fix dont know/unknown codes...set to 0
df_analysis_doctor_diabetes[,seq(2,5)][df_analysis_doctor_diabetes[,seq(2,5)]==-1]<-0 #-1 or -3  get 0
df_analysis_doctor_diabetes[,seq(2,5)][df_analysis_doctor_diabetes[,seq(2,5)]==-3]<-0 #-1 or -3  get 0
summary(as.factor(df_analysis_doctor_diabetes$doctor_diabetes_0_0))
#0     1  NA's 
#41210  1080    11 
summary(as.factor(df_analysis_doctor_diabetes$doctor_diabetes_2_0))
#0     1  NA's 
#39816  2206   279 

df_analysis_doctor_diabetes[,seq(2,5)][is.na(df_analysis_doctor_diabetes[,seq(2,5)])]<-0
summary(as.factor(df_analysis_doctor_diabetes$doctor_diabetes_2_0))
#0     1 
#40095  2206 
summary(df_analysis_doctor_diabetes$age_doctor_diabetes_2_0)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -3.00   50.00   56.00   53.31   63.00   80.00   40157 
summary(df_analysis_doctor_diabetes[which(df_analysis_doctor_diabetes$doctor_diabetes_2_0==1),]$age_doctor_diabetes_2_0)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
#   -3.00   50.00   56.00   53.31   63.00   80.00      62 

#fix dont knows/NAs..set to 0
df_analysis_doctor_diabetes[,seq(6,9)][df_analysis_doctor_diabetes[,seq(6,9)]==-1]<-0 #-1 or -3  get 0
df_analysis_doctor_diabetes[,seq(6,9)][df_analysis_doctor_diabetes[,seq(6,9)]==-3]<-0 #-1 or -3  get 0
df_analysis_doctor_diabetes[,seq(6,9)][is.na(df_analysis_doctor_diabetes[,seq(6,9)])]<-0
summary(df_analysis_doctor_diabetes[which(df_analysis_doctor_diabetes$doctor_diabetes_2_0==1),]$age_doctor_diabetes_2_0)

#get age and compute duration
df_analysis_doctor_diabetes <- list(df_analysis_doctor_diabetes, df_analysis_structural[,c("eid","Age_when_attended_assessment_centre_2_0")]) %>% reduce(left_join, by="eid")
df_analysis_doctor_diabetes$doctor_diabetes_duration_of_diabetes <- 365.25*(ifelse((df_analysis_doctor_diabetes$doctor_diabetes_2_0==1) &(df_analysis_doctor_diabetes$age_doctor_diabetes_2_0>0),
                                                                                  df_analysis_doctor_diabetes$Age_when_attended_assessment_centre_2_0 -
                                                                                    df_analysis_doctor_diabetes$age_doctor_diabetes_2_0,0))
summary(df_analysis_doctor_diabetes[which(df_analysis_doctor_diabetes$doctor_diabetes_2_0==1),]$doctor_diabetes_duration_of_diabetes)
View(df_analysis_doctor_diabetes[which(df_analysis_doctor_diabetes$doctor_diabetes_2_0==1),])
  
df_analysis_doctor_diabetes <- df_analysis_doctor_diabetes[,c(1,4,8,11)]

  
#now lets merge our three sources
df_diagnosis <- list(df_hes_diagnoses, df_analysis_selfreport) %>% reduce(left_join,by="eid")
df_diagnosis <- list(df_diagnosis, df_analysis_doctor_diabetes) %>% reduce(left_join,by="eid")
names(df_diagnosis)

#use general diabetes diagnosis
df_diagnosis$diabetes_2_0 <- ifelse( df_diagnosis$HES_diagnosis_of_diabetes_0_0 == 1 |
                                       df_diagnosis$selfreport_diabetes_2_0 == 1 |
                                       df_diagnosis$doctor_diabetes_2_0 == 1, 1, 0)
summary(as.factor(df_diagnosis$diabetes_2_0))
#0     1 
#39934  2367 

#duration
df_diabetes <- df_diagnosis[which(df_diagnosis$diabetes_2_0==1),]
summary(df_diabetes$HES_duration_of_diabetes)
summary(df_diabetes$selfreport_duration_of_diabetes)
summary(df_diabetes$doctor_diabetes_duration_of_diabetes)
#take maximum
df_diabetes$duration_of_diabetes <- pmax(df_diabetes$HES_duration_of_diabetes,
                                         df_diabetes$selfreport_duration_of_diabetes,
                                         df_diabetes$doctor_diabetes_duration_of_diabetes)
summary(df_diabetes$duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 0    1096    3064    3953    5114   27028 
hist(df_diabetes$duration_of_diabetes)
nrow(df_diabetes[which(df_diabetes$duration_of_diabetes==0),]) #these should not be included in sensitivity

df_control <- df_diagnosis[which(df_diagnosis$diabetes_2_0==0),]
df_control$duration_of_diabetes <- -1

df_diagnosis <- rbind(df_control, df_diabetes)

#lets save this data
save(df_diagnosis, file = "../../derivatives/2_df_diabetes_diagnosis.rda")
