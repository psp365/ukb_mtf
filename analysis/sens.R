#sensitivity - does duration of metformin exposure impact brain vols
library(tidyverse)
library(ggplot2)
library(rstanarm)

library(tidybayes)
library(bayesplot)
library(modelr)

library(splines)

load("../../derivatives/9_df_stats_deconfounded.rda")
names(df_stats)

summary(df_stats$metformin)
summary(df_stats$metformin_0_0)
summary(df_stats$metformin_2_0)
summary(df_stats$metformin_duration)
summary(df_stats$duration_of_diabetes)

#which idps do we focus on
basic_results <- read.csv("../results/idps_lm_results.csv", stringsAsFactors = FALSE)
sig_results <- basic_results[which( (basic_results$p_diabetes_metformin<=0.05/577) &
                                       (basic_results$preprocess=="deconfound")),]
idps_to_test <- basic_results[which( (basic_results$p_diabetes_metformin<=0.05/577) &
                                       (basic_results$preprocess=="deconfound")),"idp"]
idps_to_test


#break diabetes_metformin in two
df_stats$group_mtf_duration <- ifelse(df_stats$metformin_duration=="recent","diabetes_metformin_recent",
                         ifelse(df_stats$metformin_duration=="always","diabetes_metformin_always",
                                ifelse(df_stats$group=="control_control","control_control","diabetes_control")))
df_stats$group_mtf_duration <- as.factor(df_stats$group_mtf_duration)
summary(df_stats$group_mtf_duration)
# control_control          diabetes_control diabetes_metformin_always diabetes_metformin_recent 
# 35254                       910                       424                       674 
df_stats$group_mtf_duration <- relevel(df_stats$group_mtf_duration, "diabetes_control")

df_diabetes <- df_stats[which(df_stats$dx_group=="diabetes"),]



run_mtf_duration_sens <- function(dff, idplist, T1scale=FALSE){
  df_pvals <- data.frame(matrix(ncol=7))
  names(df_pvals) <- c("idp","b_recent","t_recent","p_recent",
                       "b_always","t_always","p_always")
  
  for (idp in idplist){
    df_model <- data.frame(matrix(ncol=7))
    names(df_model) <-c("idp","b_recent","t_recent","p_recent",
                        "b_always","t_always","p_always")
    
    if (T1scale){
      model <- lm(get(idp) ~ group_mtf_duration + cardiovascular +
                    stroke + neuro + mh +
                    Age_when_attended_assessment_centre_2_0*ukbsex + T1VolumetricScaling, data = dff)
    } else {
      model <- lm(get(idp) ~ group_mtf_duration + cardiovascular +
                    stroke + neuro + mh +
                    Age_when_attended_assessment_centre_2_0*ukbsex, data = dff)
    }
    df_model$idp <- idp
    df_model$b_recent <- round(summary(model)$coefficients[3,1],3)
    df_model$t_recent <- round(summary(model)$coefficients[3,3],1)
    df_model$p_recent <- summary(model)$coefficients[3,4]
    df_model$b_always <- round(summary(model)$coefficients[2,1],3)
    df_model$t_always <- round(summary(model)$coefficients[2,3],1)
    df_model$p_always <- summary(model)$coefficients[2,4]

    df_pvals <- rbind(df_pvals, df_model)
    
  }
  df_pvals<-df_pvals[which(!is.na(df_pvals$idp)),]
  
  return(df_pvals)
  
}

df_sensitivity_metformin_duration <- run_mtf_duration_sens(df_diabetes, idps_to_test)
df_sensitivity_metformin_duration$p_recent <- p.adjust(df_sensitivity_metformin_duration$p_recent, method='fdr')
df_sensitivity_metformin_duration$p_always <- p.adjust(df_sensitivity_metformin_duration$p_always, method='fdr')

df_sensitivity_metformin_duration$p_recent_log <- -log10(df_sensitivity_metformin_duration$p_recent)
df_sensitivity_metformin_duration$p_always_log <- -log10(df_sensitivity_metformin_duration$p_always)

write.csv(df_sensitivity_metformin_duration, "../results/sensitivity_metformin_duration_lm.csv", row.names = FALSE)



#### other meds ####
xtabs(~other_medication_2_0 + group, data = df_stats)
# group
# other_medication_2_0 diabetes_control control_control diabetes_metformin
# 0              783           35250                779
# 1              127               4                319

df_diabetes$dx_medication <- ifelse( (df_diabetes$metformin=="metformin") & (df_diabetes$other_medication_2_0==0), "diabetes_mtf",
                                     ifelse((df_diabetes$metformin=="metformin") & (df_diabetes$other_medication_2_0==1), "diabetes_mtf_mixed",
                                            ifelse( (df_diabetes$metformin=="control") & (df_diabetes$other_medication_2_0==0), "diabetes","diabetes_mixed")))
df_diabetes$dx_medication <- as.factor(df_diabetes$dx_medication)
summary(df_diabetes$dx_medication)
# diabetes     diabetes_mixed       diabetes_mtf diabetes_mtf_mixed 
# 783                127                779                319 

run_other_meds_sens <- function(dff, idplist, T1scale=FALSE){
  df_pvals <- data.frame(matrix(ncol=7))
  names(df_pvals) <- c("idp","b_mtf_pure","t_mtf_pure","p_mtf_pure",
                       "b_mtf_mixed","t_mtf_mixed","p_mtf_mixed")
  
  for (idp in idplist){
    df_model <- data.frame(matrix(ncol=7))
    names(df_model) <- c("idp","b_mtf_pure","t_mtf_pure","p_mtf_pure",
                         "b_mtf_mixed","t_mtf_mixed","p_mtf_mixed")
    
    if (T1scale){
      model <- lm(get(idp) ~ dx_medication + cardiovascular +
                    stroke + neuro + mh +
                    Age_when_attended_assessment_centre_2_0*ukbsex + T1VolumetricScaling, data = dff)
    } else {
      model <- lm(get(idp) ~ dx_medication + cardiovascular +
                    stroke + neuro + mh +
                    Age_when_attended_assessment_centre_2_0*ukbsex, data = dff)
    }
    df_model$idp <- idp
    df_model$b_mtf_pure <- round(summary(model)$coefficients[3,1],3)
    df_model$t_mtf_pure <- round(summary(model)$coefficients[3,3],1)
    df_model$p_mtf_pure <- summary(model)$coefficients[3,4]
    df_model$b_mtf_mixed <- round(summary(model)$coefficients[4,1],3)
    df_model$t_mtf_mixed <- round(summary(model)$coefficients[4,3],1)
    df_model$p_mtf_mixed <- summary(model)$coefficients[4,4]
    
    df_pvals <- rbind(df_pvals, df_model)
    
  }
  df_pvals<-df_pvals[which(!is.na(df_pvals$idp)),]
  
  return(df_pvals)
  
}

df_sensitivity_other_meds <- run_other_meds_sens(df_diabetes, idps_to_test)
df_sensitivity_other_meds$p_mtf_pure <- p.adjust(df_sensitivity_other_meds$p_mtf_pure, method='fdr')
df_sensitivity_other_meds$p_mtf_mixed <- p.adjust(df_sensitivity_other_meds$p_mtf_mixed, method='fdr')

df_sensitivity_other_meds$p_mtf_pure_log <- -log10(df_sensitivity_other_meds$p_mtf_pure)
df_sensitivity_other_meds$p_mtf_mixed_log <- -log10(df_sensitivity_other_meds$p_mtf_mixed)

write.csv(df_sensitivity_other_meds, "../results/sensitivity_other_medication_lm.csv", row.names = FALSE)

df_sensitivity <- list(df_sensitivity_metformin_duration, df_sensitivity_other_meds) %>% reduce(left_join, by="idp")

#### diabetes duration ####
summary(df_stats$duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# -1.0    -1.0    -1.0   206.9    -1.0 27028.5 

summary(df_diabetes$duration_of_diabetes)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 0    1096    2922    3857    5114   27028 

run_diabetes_duration_sens <- function(dff, idplist, T1scale=FALSE){
  df_pvals <- data.frame(matrix(ncol=4))
  names(df_pvals) <- c("idp","b_diabetes_mtf","t_diabetes_mtf","p_diabetes_mtf")
  
  for (idp in idplist){
    df_model <- data.frame(matrix(ncol=4))
    names(df_model) <- c("idp","b_diabetes_mtf","t_diabetes_mtf","p_diabetes_mtf")
    
    if (T1scale){
      model <- lm(get(idp) ~ group + duration_of_diabetes + cardiovascular +
                    stroke + neuro + mh +
                    Age_when_attended_assessment_centre_2_0*ukbsex + T1VolumetricScaling, data = dff)
    } else {
      model <- lm(get(idp) ~ group + duration_of_diabetes + cardiovascular +
                    stroke + neuro + mh +
                    Age_when_attended_assessment_centre_2_0*ukbsex, data = dff)
    }
    df_model$idp <- idp
    df_model$b_diabetes_mtf <- round(summary(model)$coefficients[2,1],3)
    df_model$t_diabetes_mtf <- round(summary(model)$coefficients[2,3],1)
    df_model$p_diabetes_mtf <- summary(model)$coefficients[2,4]

    df_pvals <- rbind(df_pvals, df_model)
    
  }
  df_pvals<-df_pvals[which(!is.na(df_pvals$idp)),]
  
  return(df_pvals)
  
}


df_sensitivity_diabetes_duration <- run_diabetes_duration_sens(df_diabetes[which(df_diabetes$duration_of_diabetes>0),], idps_to_test)
df_sensitivity_diabetes_duration$p_diabetes_mtf <- p.adjust(df_sensitivity_diabetes_duration$p_diabetes_mtf, method='fdr')
df_sensitivity_diabetes_duration$p_diabetes_mtf_log <- -log10(df_sensitivity_diabetes_duration$p_diabetes_mtf)

df_sensitivity <- list(df_sensitivity, df_sensitivity_diabetes_duration) %>% reduce(left_join, by="idp")
write.csv(df_sensitivity_diabetes_duration, "../results/sensitivity_duration.csv", row.names = FALSE)

#add method to sensitivity
df_sensitivity <- list(df_sensitivity, sig_results[,c("idp","method")]) %>% reduce(left_join, by="idp")
write.csv(df_sensitivity, "../results/sensitivity.csv", row.names = FALSE)

summary(as.factor(df_sensitivity$method))

for (method in as.factor(df_sensitivity$method)){
  d <- df_sensitivity[which(df_sensitivity$method==method),]
  write.csv(df_sensitivity[which(df_sensitivity$method==method),], paste0("../results/sensitivity_",method,".csv"), row.names = FALSE, quote = FALSE)
}



df_diabetes$mixed_meds <- ifelse( (df_diabetes$metformin=="control") & (df_diabetes$other_medication_2_0==1), "mixedmeds_nomtf",
                                     ifelse((df_diabetes$metformin=="metformin") & (df_diabetes$other_medication_2_0==0), "mtf",
                                            ifelse( (df_diabetes$metformin=="metformin") & (df_diabetes$other_medication_2_0==1), "mtf","nomeds")))
df_diabetes$mixed_meds <- as.factor(df_diabetes$mixed_meds)
summary(df_diabetes$mixed_meds)
# mixedmeds_nomtf             mtf          nomeds 
# 127                         1098             783 

summary(df_diabetes$dx_medication)
# diabetes     diabetes_mixed       diabetes_mtf diabetes_mtf_mixed 
# 783                127                779                319 

model <- lm(FIRST_left_thalamus ~ dx_medication + cardiovascular +
              stroke + neuro + mh +
              Age_when_attended_assessment_centre_2_0*ukbsex, data = df_diabetes)
summary(model)
summary(aov(model))
TukeyHSD(aov(model), which="dx_medication")


model <- lm(FIRST_left_thalamus ~ mixed_meds + cardiovascular +
              stroke + neuro + mh +
              Age_when_attended_assessment_centre_2_0*ukbsex, data = df_diabetes)
summary(model)

df_diabetes$mixed_meds <- factor(df_diabetes$mixed_meds,
                                    c("nomeds", "mixedmeds_nomtf","mtf"))

g <- ggplot(aes(x=dx_medication, y=FIRST_left_thalamus), data = df_diabetes)
g + geom_boxplot()

g <- ggplot(aes(x=mixed_meds, y=FIRST_left_thalamus), data = df_diabetes)
g + geom_boxplot()

df_diabetes$dx_medication <- relevel(df_diabetes$dx_medication, "diabetes_mixed")
df_diabetes$mixed_meds <- relevel(df_diabetes$mixed_meds, "mixedmeds_nomtf")

a <- lm(FIRST_left_thalamus ~ dx_medication + cardiovascular +
              stroke + neuro + mh +
              Age_when_attended_assessment_centre_2_0*ukbsex, data = df_diabetes)
summary(a)
# (Intercept)                                      1.981393   0.246108   8.051  1.4e-15 ***
#   dx_medicationdiabetes                            0.225427   0.071711   3.144  0.00169 ** 
#   dx_medicationdiabetes_mtf                        0.114203   0.071583   1.595  0.11078    
# dx_medicationdiabetes_mtf_mixed                 -0.033184   0.078274  -0.424  0.67165  
#compared to diabetics on meds but no mtf, diabetes on mtf no diff, diabetics on mtf and other no diff

b <- lm(FIRST_left_thalamus ~ mixed_meds + cardiovascular +
          stroke + neuro + mh +
          Age_when_attended_assessment_centre_2_0*ukbsex, data = df_diabetes)
summary(b)
# (Intercept)                                      1.954083   0.246421   7.930 3.62e-15 ***
#   mixed_medsnomeds                                 0.224453   0.071851   3.124  0.00181 ** 
#   mixed_medsmtf                                    0.070674   0.070212   1.007  0.31426  
#compared to diabetics on meds and not mtf, diabetics on mtf and anything  no diff



#quick cog
cog <- read.csv("../../raw_data/biobank_dload/ukb673825_cognition.csv", stringsAsFactors = FALSE)
cog$eid <- as.character(cog$eid)
names(cog)
sapply(cog, summary)
cog <- cog[,c(1,4,6,10,14,16)]

df_cog <- list(df_stats, cog) %>% reduce(left_join, by="eid")
sapply(df_cog[,seq(596,600)], summary)

g<-ggplot(aes(x=dx_group, y=X23324.2.0), data = df_cog)
g + geom_boxplot()

